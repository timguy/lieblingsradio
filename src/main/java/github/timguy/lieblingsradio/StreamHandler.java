/*
     Copyright 2018 Amazon.com, Inc. or its affiliates. All Rights Reserved.

     Licensed under the Apache License, Version 2.0 (the "License"). You may not use this file
     except in compliance with the License. A copy of the License is located at

         http://aws.amazon.com/apache2.0/

     or in the "license" file accompanying this file. This file is distributed on an "AS IS" BASIS,
     WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for
     the specific language governing permissions and limitations under the License.
*/

package github.timguy.lieblingsradio;

import com.amazon.ask.Skill;
import com.amazon.ask.SkillStreamHandler;
import com.amazon.ask.Skills;

import github.timguy.lieblingsradio.handlers.AudioPlayerPlaybackStarted;
import github.timguy.lieblingsradio.handlers.CancelandStopIntentHandler;
import github.timguy.lieblingsradio.handlers.HelpIntentHandler;
import github.timguy.lieblingsradio.handlers.IntentNextHandler;
import github.timguy.lieblingsradio.handlers.IntentPreviousHandler;
import github.timguy.lieblingsradio.handlers.IntentRadioNameHandler;
import github.timguy.lieblingsradio.handlers.IntentRadioNumberHandler;
import github.timguy.lieblingsradio.handlers.LaunchRequestHandler;
import github.timguy.lieblingsradio.handlers.MyExceptionHandler;
import github.timguy.lieblingsradio.handlers.SessionEndedRequestHandler;

public class StreamHandler extends SkillStreamHandler {

    private static Skill getSkill() {
        return Skills.standard()
                .addRequestHandlers(
                		new AudioPlayerPlaybackStarted(),
                		new IntentNextHandler(),
                		new IntentPreviousHandler(),
                        new IntentRadioNameHandler(),
                        new IntentRadioNumberHandler(),
                        new LaunchRequestHandler(),
                        new CancelandStopIntentHandler(),
                        new SessionEndedRequestHandler(),
                        new HelpIntentHandler())
                // Add your skill id below
                .addExceptionHandler(new MyExceptionHandler())
                //.withSkillId(appId2) we do have multiple app ids and the API can not cope with that
                .build();
    }

    public StreamHandler() {
        super(getSkill());
    }

}
