package github.timguy.lieblingsradio;

import java.util.Arrays;
import java.util.List;
import java.util.NavigableMap;
import java.util.TreeMap;

public class RadioChannel {

	private int number;
	private String name;
	private String url;
	private List<String> keywords;

	private static NavigableMap<Integer, RadioChannel> channelMap;

	public RadioChannel(int number, String name, String url,
			String... keywords) {
		super();
		this.number = number;
		this.name = name;
		this.url = url;
		this.keywords = Arrays.asList(keywords);
	}

	public static NavigableMap<Integer, RadioChannel> getAllChannels() {
		if (channelMap == null) {
			channelMap = new TreeMap<>();

			RadioChannel radioChannel = new RadioChannel(1,
					"MDR Sputnik Insomnia Channel",
					"http://mdr-284331-3.cast.mdr.de/mdr/284331/3/mp3/high/stream.mp3",
					"Insomnia");
			channelMap.put(radioChannel.getNumber(), radioChannel);

			radioChannel = new RadioChannel(2, "JamendoLounge Chillout",
					"https://streaming.radionomy.com/JamendoLounge",
					"JamendoLounge", "Chillout");
			channelMap.put(radioChannel.getNumber(), radioChannel);
			
			radioChannel = new RadioChannel(3, "Flux FM Clubsandwich",
					"https://streams.fluxfm.de/clubsandwich/mp3-320/streams.fluxfm.de/", "Flux", "Flax",
					"Flax FM");
			channelMap.put(radioChannel.getNumber(), radioChannel);
			
			radioChannel = new RadioChannel(4, "Detektor FM",
					"https://dtfm-fip-02.ezpz.io/detektor-fm-musik.mp3",
					"Detektor", "Leipzig", "Pop");
			channelMap.put(radioChannel.getNumber(), radioChannel);
			
			radioChannel = new RadioChannel(5, "Detektor FM Wortstream",
					"https://sec-detektorfm.hoerradar.de/detektorfm-wort-mp3-128",
					"Detektor", "Leipzig", "Pop");
			channelMap.put(radioChannel.getNumber(), radioChannel);
			
			
		
				// Chicago Polie dep - https://www.broadcastify.com/listen/feed/17693/web
				//http://relay.broadcastify.com/1m65qbnhp7yg94c.mp3  
				
				
			/**
			 * <td width="33%" valign="top" align="left">Status: <img src="status.gif" alt="is on air" title="is on air" style="margin-bottom: -1px;" width="40" height="10"></td>
			 */
//			radioChannel = new RadioChannel(6, "Lokruf",
//					"http://lokruf.onlinestream.de/listen1.m3u",
//					"Lokruf", "Leipzig", "Sport");
//			channelMap.put(radioChannel.getNumber(), radioChannel);
			
				
			radioChannel = new RadioChannel(6, "Space Travel", "https://spacetravelradio.de/listen_str.m3u",
					"Space, Elektro");
			channelMap.put(radioChannel.getNumber(), radioChannel);

			radioChannel = new RadioChannel(7, "Flugfunk",
					"https://gist.githubusercontent.com/timguy/a7a5fc5e3e1c785e34d166e8bc696b95/raw/84ff386b6903737feb91a60ab396a515e3efed1c/Flugfunk_NewYork.m3u",
					"Flugfunk");
			channelMap.put(radioChannel.getNumber(), radioChannel);

			//mit Anfangswerbungs
			radioChannel = new RadioChannel(50, "Tannenbaumradio",
					"https://stream.laut.fm/tannenbaumradio", "Weihnachten");
			channelMap.put(radioChannel.getNumber(), radioChannel);
			
			//geht
			radioChannel = new RadioChannel(8, "Tannenbaum FFN",
					"https://stream.ffn.de/tannenbaum/mp3-192/", "Weihnachten");
			channelMap.put(radioChannel.getNumber(), radioChannel);
			
			//radio roberto -
			//only available as http:	http://64.71.79.181:5238/stream
			//but packed in a m3u file? vlc works, alexa: geht
			
			radioChannel = new RadioChannel(9, "radio roberto",
					"https://gist.githubusercontent.com/timguy/cc67df71e36e0698cf81084ce9f3488b/raw/5f1eb34b578d475641e1adc2e7fcecce0100b527/stream.m3u"
					, "free music");
			channelMap.put(radioChannel.getNumber(), radioChannel);

			
			radioChannel = new RadioChannel(10, "SPUTNIK Rock Channel",
					"https://avw.mdr.de/streams/284331-1_mp3_high.m3u",
					"SPUTNIK Rock Channel");
			channelMap.put(radioChannel.getNumber(), radioChannel);
			
			radioChannel = new RadioChannel(11, "SPUTNIK Insomnia Channel",
					"https://avw.mdr.de/streams/284331-3_mp3_high.m3u",
					"SPUTNIK Insomnia Channel");
			channelMap.put(radioChannel.getNumber(), radioChannel);
			
			
			radioChannel = new RadioChannel(12, "SPUTNIK Soundcheck Channel",
					"https://avw.mdr.de/streams/284331-6_mp3_high.m3u",
					"SPUTNIK Soundcheck Channel");
			channelMap.put(radioChannel.getNumber(), radioChannel);
			
			radioChannel = new RadioChannel(13, "SPUTNIK OnAir Channel",
					"https://avw.mdr.de/streams/284331-1_mp3_high.m3u",
					"SPUTNIK OnAir Channel");
			channelMap.put(radioChannel.getNumber(), radioChannel);
			
			radioChannel = new RadioChannel(14, "SPUTNIK Roboton Channel",
					"https://avw.mdr.de/streams/284331-5_mp3_high.m3u",
					"SPUTNIK Roboton Channel");
			channelMap.put(radioChannel.getNumber(), radioChannel);
			
			radioChannel = new RadioChannel(15, "SPUTNIK Club Channel",
					"https://avw.mdr.de/streams/284331-2_mp3_high.m3u",
					"SPUTNIK Club Channel");
			channelMap.put(radioChannel.getNumber(), radioChannel);
			
			radioChannel = new RadioChannel(16, "SPUTNIK Black Channel",
					"http://avw.mdr.de/streams/284331-0_mp3_high.m3u",
					"SPUTNIK Black Channel");
			channelMap.put(radioChannel.getNumber(), radioChannel);
			
			radioChannel = new RadioChannel(17, "SPUTNIK Club Channel",
					"https://avw.mdr.de/streams/284331-2_mp3_high.m3u",
					"SPUTNIK Club Channel");
			channelMap.put(radioChannel.getNumber(), radioChannel);
			
			radioChannel = new RadioChannel(18, "SPUTNIK Popkult Channel",
					"https://avw.mdr.de/streams/284331-4_mp3_high.m3u",
					"SPUTNIK Popkult Channel");
			channelMap.put(radioChannel.getNumber(), radioChannel);
			
			radioChannel = new RadioChannel(19, "Corax",
					"http://streaming.fueralle.org:8000/corax.mp3.m3u",
					"corax", "Halle");
			channelMap.put(radioChannel.getNumber(), radioChannel);
			
			
			
			radioChannel = new RadioChannel(20, "D RadioWissen",
					"http://www.deutschlandradio.de/streaming/dradiowissen.m3u",
					"RadioWissen");
			channelMap.put(radioChannel.getNumber(), radioChannel);
			radioChannel = new RadioChannel(21, "NDR Info",
					"https://www.ndr.de/resources/metadaten/audio/m3u/ndrinfo_hh.m3u",
					"NDR Info");
			channelMap.put(radioChannel.getNumber(), radioChannel);
			
			radioChannel = new RadioChannel(30, "Energy",
					"http://cdn.nrjaudio.fm/adwz1/de/33005/mp3_128.mp3",
					"Energy");
			channelMap.put(radioChannel.getNumber(), radioChannel);
			
			
			
		}
		return channelMap;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public List<String> getKeywords() {
		return keywords;
	}

	public void setKeywords(List<String> keywords) {
		this.keywords = keywords;
	}

	public int getNumber() {
		return number;
	}

	public void setNumber(int number) {
		this.number = number;
	}

	@Override
	public String toString() {
		return number + " " + name;
	}

}
