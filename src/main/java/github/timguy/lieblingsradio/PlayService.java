package github.timguy.lieblingsradio;

import java.util.Optional;

import com.amazon.ask.dispatcher.request.handler.HandlerInput;
import com.amazon.ask.model.Response;
import com.amazon.ask.model.interfaces.audioplayer.PlayBehavior;
import com.amazon.ask.response.ResponseBuilder;

public class PlayService {

	public static Optional<Response> replyWithStream(Integer channelNumber, HandlerInput input) {
		ResponseBuilder responseBuilder = input.getResponseBuilder();
		String speechText;
		RadioChannel radioChannel = RadioChannel.getAllChannels()
				.get(channelNumber);

		responseBuilder.addAudioPlayerPlayDirective(
				PlayBehavior.REPLACE_ALL, 0l, "",
				String.valueOf(radioChannel.getNumber()),
				radioChannel.getUrl());

		speechText = radioChannel.toString();
		responseBuilder.withSimpleCard("Play ", speechText)
				.withSpeech(speechText)
				.withShouldEndSession(true);

		return responseBuilder.build();
	}

}
