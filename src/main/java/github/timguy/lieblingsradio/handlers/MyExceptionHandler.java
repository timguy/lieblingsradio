package github.timguy.lieblingsradio.handlers;

import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.amazon.ask.dispatcher.exception.ExceptionHandler;
import com.amazon.ask.dispatcher.request.handler.HandlerInput;
import com.amazon.ask.model.Response;

public class MyExceptionHandler implements ExceptionHandler {
    
	private static final Logger log = LoggerFactory
			.getLogger(MyExceptionHandler.class);
	
	@Override
    public boolean canHandle(HandlerInput input, Throwable throwable) {
        //return throwable instanceof AskSdkException;
        return true;
    }

	@Override
	public Optional<Response> handle(HandlerInput input, Throwable throwable) {
		log.error("Error catched: " + input.getRequestEnvelope().toString(), throwable);
		
		return input.getResponseBuilder()
				.withSpeech("Da gabs nen Fehler: " + throwable.getClass().getName() + " Im Detail: " + throwable.getLocalizedMessage())
				.build();
	}
}