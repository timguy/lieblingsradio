/*
     Copyright 2018 Amazon.com, Inc. or its affiliates. All Rights Reserved.

     Licensed under the Apache License, Version 2.0 (the "License"). You may not use this file
     except in compliance with the License. A copy of the License is located at

         http://aws.amazon.com/apache2.0/

     or in the "license" file accompanying this file. This file is distributed on an "AS IS" BASIS,
     WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for
     the specific language governing permissions and limitations under the License.
*/

package github.timguy.lieblingsradio.handlers;

import com.amazon.ask.dispatcher.request.handler.HandlerInput;
import com.amazon.ask.dispatcher.request.handler.RequestHandler;
import com.amazon.ask.model.Response;
import com.amazon.ask.model.interfaces.audioplayer.PlaybackStartedRequest;
import com.amazon.ask.model.interfaces.audioplayer.PlaybackStoppedRequest;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static com.amazon.ask.request.Predicates.intentName;
import static com.amazon.ask.request.Predicates.requestType;

public class CancelandStopIntentHandler implements RequestHandler {
	
	private static final List<String> goodByeSpeeches = Arrays.asList("Tschö mit ö",
			"Tschüssel du Düssel", "Ja ist ja gut - Ruhe", "Weg bin ich",
			"Laut ist schön, aber nicht immer",
			"Ok, in 5 Minuten gibts aber wieder was auf die Ohren",
			"Ach war doch grad so schön", "Ich bin leise du bist leise",
			"See you later alligator", "Bis denne du Henne",
			"Komm bald wieder", "Schweigen ist gold", "Und wee wie weg", "Ok, Ruhe");
	
	@Override
	public boolean canHandle(HandlerInput input) {
		return input.matches(requestType(PlaybackStoppedRequest.class)
				.or(intentName("AMAZON.StopIntent")
						.or(intentName("AMAZON.CancelIntent")
								.or(intentName("AMAZON.PauseIntent")))));
	}

	@Override
	public Optional<Response> handle(HandlerInput input) {

		return input.getResponseBuilder()
				.withSpeech(getRandomGoodBye())
				.addAudioPlayerStopDirective()
				.build();
	}

	public static String getRandomGoodBye() {
		return goodByeSpeeches.stream()
				.skip((int) (goodByeSpeeches.size() * Math.random()))
				.findFirst().get();
	}
}
