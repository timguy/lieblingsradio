/*
     Copyright 2018 Amazon.com, Inc. or its affiliates. All Rights Reserved.

     Licensed under the Apache License, Version 2.0 (the "License"). You may not use this file
     except in compliance with the License. A copy of the License is located at

         http://aws.amazon.com/apache2.0/

     or in the "license" file accompanying this file. This file is distributed on an "AS IS" BASIS,
     WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for
     the specific language governing permissions and limitations under the License.
*/

package github.timguy.lieblingsradio.handlers;

import com.amazon.ask.dispatcher.request.handler.HandlerInput;
import com.amazon.ask.dispatcher.request.handler.RequestHandler;
import com.amazon.ask.model.Intent;
import com.amazon.ask.model.IntentRequest;
import com.amazon.ask.model.Request;
import com.amazon.ask.model.Response;
import com.amazon.ask.model.Slot;
import com.amazon.ask.model.interfaces.audioplayer.AudioPlayerInterface;
import com.amazon.ask.model.interfaces.audioplayer.PlayBehavior;
import com.amazon.ask.model.services.directive.Directive;
import com.amazon.ask.model.services.directive.DirectiveServiceClient;
import com.amazon.ask.model.services.directive.SendDirectiveRequest;
import com.amazon.ask.response.ResponseBuilder;

import github.timguy.lieblingsradio.RadioChannel;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Optional;

import static com.amazon.ask.request.Predicates.intentName;

public class IntentNextHandler implements RequestHandler {

	@Override
	public boolean canHandle(HandlerInput input) {
		return input.matches(intentName("AMAZON.NextIntent"));
	}

	@Override
	public Optional<Response> handle(HandlerInput input) {
		Request request = input.getRequestEnvelope().getRequest();
		IntentRequest intentRequest = (IntentRequest) request;
		Intent intent = intentRequest.getIntent();

		String token = input.getRequestEnvelope().getContext().getAudioPlayer()
				.getToken();
		int oldChannelNumber = Integer.valueOf(token);
		Entry<Integer, RadioChannel> higherEntry = RadioChannel.getAllChannels()
				.higherEntry(oldChannelNumber);
		RadioChannel radioChannel;
		// at the end of the map, no higher keys
		if (higherEntry == null) {
			radioChannel = RadioChannel.getAllChannels().get(1);
		} else {
			radioChannel = higherEntry.getValue();
		}

		ResponseBuilder responseBuilder = input.getResponseBuilder();
		responseBuilder.addAudioPlayerPlayDirective(PlayBehavior.REPLACE_ALL,
				0l, "", String.valueOf(radioChannel.getNumber()),
				radioChannel.getUrl());

		String speechText = radioChannel.getNumber() + " " + radioChannel.getName();
		responseBuilder.withSimpleCard("Play ", speechText)
				.withSpeech(speechText)
				.withShouldEndSession(true);

		return responseBuilder.build();
	}
}
