/*
     Copyright 2018 Amazon.com, Inc. or its affiliates. All Rights Reserved.

     Licensed under the Apache License, Version 2.0 (the "License"). You may not use this file
     except in compliance with the License. A copy of the License is located at

         http://aws.amazon.com/apache2.0/

     or in the "license" file accompanying this file. This file is distributed on an "AS IS" BASIS,
     WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for
     the specific language governing permissions and limitations under the License.
*/

package github.timguy.lieblingsradio.handlers;

import com.amazon.ask.dispatcher.request.handler.HandlerInput;
import com.amazon.ask.dispatcher.request.handler.RequestHandler;
import com.amazon.ask.model.Response;

import github.timguy.lieblingsradio.RadioChannel;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.NavigableMap;
import java.util.Optional;

import static com.amazon.ask.request.Predicates.intentName;

public class HelpIntentHandler implements RequestHandler {
    @Override
    public boolean canHandle(HandlerInput input) {
        return input.matches(intentName("AMAZON.HelpIntent").or(intentName("IntentChannelList")));
    }

    @Override
    public Optional<Response> handle(HandlerInput input) {
    	
    	StringBuffer speechText = new StringBuffer("Hier ist die Senderliste. ");
    	NavigableMap<Integer, RadioChannel> allChannels = RadioChannel.getAllChannels();
		for(Map.Entry<Integer, RadioChannel> radio: allChannels.entrySet()) {
    		speechText.append(radio.getKey());
    		speechText.append(" ");
    		speechText.append( radio.getValue().getName());
    		speechText.append("<break time=\"0.3s\"/>");
    	}
		
		speechText.append("<audio src='https://s3.amazonaws.com/ask-soundlibrary/transportation/amzn_sfx_bicycle_bell_ring_01.mp3'/>");
        String repromptText = "Nenn mir ne Nummer";
        return input.getResponseBuilder()
                .withSimpleCard("Senderliste", speechText.toString())
                .withSpeech(speechText.toString())
                .withReprompt(repromptText)
                .withShouldEndSession(false)
                .build();
    }
}
