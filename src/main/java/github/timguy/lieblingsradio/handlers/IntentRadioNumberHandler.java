/*
     Copyright 2018 Amazon.com, Inc. or its affiliates. All Rights Reserved.

     Licensed under the Apache License, Version 2.0 (the "License"). You may not use this file
     except in compliance with the License. A copy of the License is located at

         http://aws.amazon.com/apache2.0/

     or in the "license" file accompanying this file. This file is distributed on an "AS IS" BASIS,
     WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for
     the specific language governing permissions and limitations under the License.
*/

package github.timguy.lieblingsradio.handlers;

import com.amazon.ask.dispatcher.request.handler.HandlerInput;
import com.amazon.ask.dispatcher.request.handler.RequestHandler;
import com.amazon.ask.model.Intent;
import com.amazon.ask.model.IntentRequest;
import com.amazon.ask.model.Request;
import com.amazon.ask.model.Response;
import com.amazon.ask.model.Slot;
import com.amazon.ask.model.interfaces.audioplayer.AudioPlayerInterface;
import com.amazon.ask.model.interfaces.audioplayer.PlayBehavior;
import com.amazon.ask.model.services.directive.Directive;
import com.amazon.ask.model.services.directive.DirectiveServiceClient;
import com.amazon.ask.model.services.directive.SendDirectiveRequest;
import com.amazon.ask.response.ResponseBuilder;

import github.timguy.lieblingsradio.PlayService;
import github.timguy.lieblingsradio.RadioChannel;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

import static com.amazon.ask.request.Predicates.intentName;

public class IntentRadioNumberHandler implements RequestHandler {

	public static final String CURRENT_CHANNEL_NUMMBER = "CurrentChannelNummber";
	public static final String SLOTCHANNELNUMBER = "SlotChannelNumber";

	@Override
	public boolean canHandle(HandlerInput input) {
		return input.matches(intentName("IntentRadioNumber"));
	}

	@Override
	public Optional<Response> handle(HandlerInput input) {
		Request request = input.getRequestEnvelope().getRequest();
		IntentRequest intentRequest = (IntentRequest) request;
		Intent intent = intentRequest.getIntent();
		Map<String, Slot> slots = intent.getSlots();

		// Get the color slot from the list of slots.
		Slot channelNumberSlot = slots.get(SLOTCHANNELNUMBER);

		String speechText, repromptText;
		boolean isAskResponse = false;

		ResponseBuilder responseBuilder = input.getResponseBuilder();
		// Check for favorite color and create output to user.
		if (channelNumberSlot != null) {
			// Store the user's favorite color in the Session and create
			// response.
			String numberString = channelNumberSlot.getValue();

			input.getAttributesManager().setSessionAttributes(
					Collections.singletonMap(CURRENT_CHANNEL_NUMMBER,
							channelNumberSlot));

			Integer number = null;
			try {
				number = Integer.valueOf(numberString);
			} catch (NumberFormatException e) {
				responseBuilder
						.withSpeech(
								"Konnte die Zahl nicht erkennen. Versuch's nomal")
						.withShouldEndSession(false)
						.withReprompt("Sag mir nochmal ne Nummer");
				return responseBuilder.build();
			}
			return PlayService.replyWithStream(number, input);

		} else {
			// Render an error since we don't know what the users favorite color
			// is.
			speechText = "Konnte keine Nummer erkennen.";
			repromptText = "Sag mit nochmal ne Nummer";
			isAskResponse = true;

			responseBuilder.withSimpleCard("Nummer Session", speechText)
					.withSpeech(speechText)
					.withShouldEndSession(false)
					.withReprompt(repromptText);
			return responseBuilder.build();
		}

	}


}
